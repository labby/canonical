<?php

/**
 * @module          Canonical
 * @author          cms-lab
 * @copyright       2017-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/canonical/license.php
 * @license_terms   please see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php


if(isset ($_GET['tool'])) {
	$toolname = $_GET['tool'];
} else {
	die('[1]');
}

// get instance of functions file
$oCF = canonical::getInstance();

// delete obsolete entries
foreach ($oCF->modified_pages as $existing) 
{
	if($database->get_one("SELECT page_id FROM ".TABLE_PREFIX."pages WHERE page_id = ".$existing['page_id']." ") == null) 
	{
		$database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_canonical WHERE page_id = ".$existing['page_id']."");		
	}
}				

if(isset ($_GET['tool']) && (empty($_POST)) ) 
{
	$oCF->list_pages();
}

if(isset ($_POST['show_info']) && ($_POST['show_info']== 'show') ) 
{
	$oCF->show_info();
}

if(isset ($_POST['add_entry']) ) 
{
	$oCF->edit_link('new');
}

if(isset ($_POST['save_entry']) ) 
{
	$oCF->save_link('entry');
}

if(isset ($_POST['save_link']) ) 
{
	$oCF->save_link('link');
}

if(isset ($_POST['edit_link']) ) 
{
	$oCF->edit_link('edit');
}

if(isset ($_POST['delete_link']) ) 
{
	$oCF->delete_link();
}
