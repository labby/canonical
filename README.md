### Canonical
============

A LEPTON tool to easily edit and set canonical links.

#### Requirements

* [LEPTON CMS][1], Version => see precheck.php inside the addon.


#### Installation

* download latest [.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon you are done. <br />
Please go to "Admintools" in the backend and use it!

For further informations please read [the readme file][3]


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/admintools/canonical.php
[3]: http://cms-lab.com/_documentation/canonical/readme.php