<?php

/**
 * @module          Canonical
 * @author          cms-lab
 * @copyright       2017-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/canonical/license.php
 * @license_terms   please see license
 *
 */

class canonical extends LEPTON_abstract
{
	public array $pages = [];
	public array $modified_pages = [];
	public array $not_modified_pages = [];
	public string $addon_color = "blue";
	public string $support_link = "<a href=\"#\">NO Live-Support / FAQ</a>";
	public string $readme_link =  "<a href=\"https://cms-lab.com/_documentation/canonical/readme.php \" class=\"info\"target=\"_blank\">Readme</a>";
	public string $action_url = "";	

	public object|null $oTwig = null;
	public LEPTON_admin $admin;
	public LEPTON_database $database;
	static $instance;

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
		$this->admin = LEPTON_admin::getInstance();	
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('canonical');
		$this->action_url = ADMIN_URL."/admintools/tool.php?tool=canonical";
		$this->init_tool();
	}
	
	public function init_tool( $sToolname = '' )
	{
		//get all pages
		$this->database->execute_query(
			"SELECT page_id, page_title, menu_title FROM ".TABLE_PREFIX."pages ORDER BY page_id " ,
			true,
			$this->pages,
			true
		);		

		//get all modified pages
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_canonical ORDER BY page_id " ,
			true,
			$this->modified_pages,
			true
		);

		//get all not modified pages 
		foreach( $this->pages as $display_page) 
		{
			$found = false;
			foreach($this->modified_pages as $test_page) {
				if($test_page['page_id'] == $display_page['page_id'] ) 
				{
					$found = true;
					break;
				}
			}
			if($found === false) 
			{
				$this->not_modified_pages[] = $display_page;
			}
		}			
	}

    public function list_pages() 
	{
		// delete obsolete entries
		foreach ($this->modified_pages as $existing) 
		{
			if($this->database->get_one("SELECT page_id FROM ".TABLE_PREFIX."pages WHERE page_id = ".$existing['page_id']." ") == NULL) {
				$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_canonical WHERE page_id = ".$existing['page_id']);		
			}
		}	

		// data for twig template engine	
		$data = array(
			'oCF'			=> $this,			
			'readme_link'	=> "https://cms-lab.com/_documentation/canonical/readme.php",	
			'leptoken'		=> get_leptoken()		

			);

		// get the template-engine	
		echo $this->oTwig->render( 
			"@canonical/list.lte",	//	template-filename
			$data						//	template-data
		);
	
	}	

    public function show_info() 
	{
		// data for twig template engine	
		$data = array(
			'oCF'			=> $this,
			'image_url'		=> 'https://cms-lab.com/_documentation/media/canonical/canonical.jpg'
			);

		// get the template-engine	
		echo $this->oTwig->render(
			"@canonical/info.lte",	//	template-filename
			$data						//	template-data
		);		
	}

	function save_link($id) 
	{
		if ( $id == 'entry') 
		{
			if( ($_POST['save_entry'] == 'cancel') || ($_POST['page_id'] == '') ) 
			{
				header("Location: ".$this->action_url."&leptoken=".get_leptoken() );
				exit(0);	
			}			

			if( ($_POST['save_entry'] == 'save') && ($_POST['page_id'] != '') ) 
			{
				
				$existing_id = $this->database->get_one("SELECT page_id FROM ".TABLE_PREFIX."mod_canonical WHERE page_id=".$_POST['page_id']);
				if($existing_id != null)
				{
					header("Location: ".$this->action_url."&leptoken=".get_leptoken() );
					exit(0);
				} 
				else 
				{
					// insert values
					$values = [];
					$this->database->execute_query(
						"SELECT * FROM ".TABLE_PREFIX."pages WHERE page_id =".$_POST['page_id'],
						true,
						$values,
						false
					);				
					$this->database->simple_query("INSERT INTO ".TABLE_PREFIX."mod_canonical VALUES (NULL, ".$values['page_id'].", '".$values['page_title']."', '".$values['menu_title']."', '".LEPTON_URL."')");
					$this->admin->print_success($this->language['record_saved'], $this->action_url);
				}				
			}
			// Print admin footer
			$this->admin->print_footer();
		}
		
		if ( $id == 'link') 
		{
			if( ($_POST['save_link'] == 'cancel') || ($_POST['new_url'] == '') ) {
				header("Location: ".$this->action_url."&leptoken=".get_leptoken() );
				exit(0);	
			}

			if(isset ($_POST['save_link']) && is_numeric ($_POST['save_link']) ) 
			{	
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_canonical SET new_url = '".$_POST['new_url']."' WHERE id = ".$_POST['save_link']);
				$this->admin->print_success($this->language['record_saved'], $this->action_url);
			}
			// Print admin footer
			$this->admin->print_footer();		
		}
	}
	
	function edit_link($id) 
	{
		if ( $id == 'new') 
		{
			// data for twig template engine	
			$data = array(
				'oCF'			=> $this,					
				'leptoken'		=> get_leptoken()					
				);

			// get the template-engine	
			echo $this->oTwig->render(
				"@canonical/add.lte",	//	template-filename
				$data							//	template-data
			);			
		}		

		if ( $id == 'edit') 
		{	
			if(isset ($_POST['edit_link']) && is_numeric ($_POST['edit_link']) ) 
			{
				$edit_id = $_POST['edit_link'];
			}

			$current = $this->database->get_one("SELECT new_url FROM ".TABLE_PREFIX."mod_canonical WHERE id = ".$edit_id);	
			
			// data for twig template engine	
			$data = array(
				'oCF'			=> $this,			
				'current_url'	=> $current,
				'current_id'	=> $edit_id,
				'leptoken'		=> get_leptoken()		
				);

			// get the template-engine	
			echo $this->oTwig->render(
				"@canonical/edit.lte",	//	template-filename
				$data					//	template-data
			);
		}
	}

	function delete_link() 
	{
		if(isset ($_POST['delete_link']) && is_numeric ($_POST['delete_link']) ) 
		{
			$to_delete = $_POST['delete_link'];
		}
		
		$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_canonical WHERE id = '".$to_delete);		
		$this->admin->print_success($this->language['record_deleted'], $this->action_url);

		// Print admin footer
		$this->admin->print_footer();	
	}
} // end of class
