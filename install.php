<?php

/**
 * @module          Canonical
 * @author          cms-lab
 * @copyright       2017-2024 cms-lab
 * @link            https://cms-lab.com
 * @license         custom license: https://cms-lab.com/_documentation/canonical/license.php
 * @license_terms   please see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.phpp


//	create table
$table_fields="
	`id` INT NOT NULL AUTO_INCREMENT,
	`page_id` INT(11) NOT NULL DEFAULT -1,
	`page_title` varchar(255) NOT NULL DEFAULT '',
	`menu_title` varchar(255) NOT NULL DEFAULT '',
	`new_url` varchar(255) NOT NULL DEFAULT '".LEPTON_URL."',
	 PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_canonical", $table_fields);

// import default droplet
LEPTON_handle::install_droplets('canonical', 'droplet_canonical-link');
